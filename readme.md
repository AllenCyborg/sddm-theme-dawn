![Preview.png](https://gitlab.com/es20490446e/sddm-theme-dawn/-/raw/main/root/usr/share/sddm/themes/dawn/Preview.png)

Also available on:
- KDE System Settings -> Startup and Shutdown -> Login Screen (SDDM)
- [The KDE Store](https://store.kde.org/p/1939296)
- [The Express Repository](https://gitlab.com/es20490446e/express-repository/-/wikis/home)

